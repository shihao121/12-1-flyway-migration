CREATE TABLE `service`
(
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `type` VARCHAR(32) NOT NULL
);

ALTER TABLE `contract` ADD `service_id` INT;
ALTER TABLE `contract` ADD FOREIGN KEY (`service_id`) REFERENCES `service` (`id`)
