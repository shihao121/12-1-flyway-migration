CREATE TABLE `branch` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(128) NOT NULL ,
    `client_id` INT,
    FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
)