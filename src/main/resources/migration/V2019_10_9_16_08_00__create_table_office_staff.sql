CREATE TABLE `office` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `country` VARCHAR(32) NOT NULL ,
    `city` VARCHAR(32) NOT NULL
);

CREATE TABLE `staff` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `last_name` VARCHAR(32) NOT NULL ,
    `first_name` VARCHAR(32) NOT NULL ,
    `office_id` INT,
    FOREIGN KEY (`office_id`) REFERENCES `office` (`id`)
)