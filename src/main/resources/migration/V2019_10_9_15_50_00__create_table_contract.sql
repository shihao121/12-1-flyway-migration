CREATE TABLE `contract` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(128) NOT NULL ,
    `branch_id` INT,
    FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`)
)