CREATE TABLE `contract_service_office` (
    `contract_id` INT NOT NULL ,
    `office_id` INT NOT NULL ,
    FOREIGN KEY (`contract_id`) REFERENCES `contract` (`id`),
    FOREIGN KEY (`office_id`) REFERENCES `office` (`id`),
    PRIMARY KEY (`contract_id`, `office_id`)
)